// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function b = %DBLDBL_cos ( a )
    // Computes cos
    //
    // Calling Sequence
    // ddc = cos(dda)
    //
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"))

    [ddx,ddy] = dbldbl_ddcssn ( a.dd )
    b = dbldbl_new(ddx)
endfunction


