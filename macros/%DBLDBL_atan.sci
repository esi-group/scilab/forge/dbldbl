// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function a = %DBLDBL_atan ( varargin )
    // Computes atan
    //
    // Calling Sequence
    // dda = atan(x)
    // dda = atan(y,x)
    //
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs,rhs]=argn()
    if ( rhs == 1 ) then
      x = varargin(1)
      t = x/sqrt(x^2+1)
      a = asin(t)
    else
      y = varargin(1)
      x = varargin(2)
      dda = dbldbl_ddangle ( x.dd, y.dd )
      a = dbldbl_new(dda)
    end
endfunction


