// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function b = dbldbl_tostr ( varargin )
    // Convert to decimal
    //
    // Calling Sequence
    // str = dbldbl_tostr ( a )
    // str = dbldbl_tostr ( a , fmt)
    // str = dbldbl_tostr ( a , fmt, n1)
    // str = dbldbl_tostr ( a , fmt, n1, n2)
    //
    // Parameters
    // a : a double-double
    // fmt : a 1-by-1 matrix of strings, the format. fmt="e" or fmt="f".
    // n1 : a 1-by-1 matrix of doubles, the total number of characters  (default n1=40)
    // n2 : a 1-by-1 matrix of doubles, the total number of characters after the decimal point  (default n2=30)
    // str : a 1-by-1 matrix of strings, the decimal representation of a
    //
    // Description
    //   If no fmt argument is given, converts the DD number A into string form in the string str, with 40 characters.
    //   The format is analogous to the Fortran E format.
    //
    //   If fmt="e", converts the DD number A to E format, i.e. En1.n2.
    //   B a n1-by-1 matrix of strings, with length 40.
    //   N1 must exceed N2 by at least 8, and N1 must not exceed 80.  
    //   N2 must not exceed 30, i.e., not more than 31 significant digits.
    //
    //   If fmt="f", converts the DD number A to F format, i.e. Fn1.n2.
    //   B a 1-by-1 matrix of strings, with length 40.  
    //   N1 must exceed N2 by at least 3, and N1 must not exceed 80.  
    //   N2 must not exceed 30.
    //
    //   If the input arguments are valid, but the data cannot be formatted, 
    //   then "*" characters are put in the output string.
    //
    // Examples
    // a = dbldbl_new ( 1.23456789123456789 , 1.2345678912345678e-16 );
    // str = dbldbl_tostr ( a )
    // str = dbldbl_tostr ( a , "e" )
    // str = dbldbl_tostr ( a , "f" )
    // str = dbldbl_tostr ( a , "e", 30 , 20 )
    // str = dbldbl_tostr ( a , "f", 30 , 20 )
    //
    // // See with large numbers
    // a = dbldbl_new ( -1.23456789123456789e-123 )
    // dbldbl_tostr(a,"f")
    // dbldbl_tostr(a,"e")
    // dbldbl_tostr(a,"e",30,15)
    //
    // // See this in action
    // a = dbldbl_new ( -1.23456789123456789e-123 )
    // for n2 = 1 : 30
    //   n1 = n2 + 8;
    //   s = dbldbl_tostr(a,"e",n1,n2);
    //   mprintf("n1=%d,n2=%d, s=%s\n",n1,n2,s);
    // end
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    function argin = argindefault ( rhs , vararglist , ivar , default )
        // Returns the value of the input argument #ivar.
        // If this argument was not provided, or was equal to the 
        // empty matrix, returns the default value.
        if ( rhs < ivar ) then
            argin = default
        else
            if ( vararglist(ivar) <> [] ) then
                argin = vararglist(ivar)
            else
                argin = default
            end
        end
    endfunction

    // Load Internals lib
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs,rhs]=argn()
    apifun_checkrhs ( "dbldbl_tostr" , rhs , 1:4 )
    apifun_checklhs ( "dbldbl_tostr" , lhs , 0:1 )
    //
    a = varargin(1)
    fmt = argindefault ( rhs , varargin , 2 , "e" )
    n1 = argindefault ( rhs , varargin , 3 , 40 )
    n2 = argindefault ( rhs , varargin , 4 , 30 )
    //
    // Check type
    apifun_checktype ( "dbldbl_tostr" , a , "a" , 1 , "DBLDBL" )
    apifun_checktype ( "dbldbl_tostr" , fmt , "fmt" , 2 , "string" )
    apifun_checktype ( "dbldbl_tostr" , n1 , "n1" , 3 , "constant" )
    apifun_checktype ( "dbldbl_tostr" , n2 , "n2" , 4 , "constant" )
    //
    // Check size
    apifun_checkscalar ( "dbldbl_tostr" , fmt , "fmt" , 2 )
    apifun_checkscalar ( "dbldbl_tostr" , n1 , "n1" , 3 )
    apifun_checkscalar ( "dbldbl_tostr" , n2 , "n2" , 4 )
    //
    // Check content
    apifun_checkoption ( "dbldbl_tostr" , fmt , "fmt" , 2 , ["e" "f"] )
    apifun_checkgreq ( "dbldbl_tostr" , n1 , "n1" , 3 , 1 )
    apifun_checkgreq ( "dbldbl_tostr" , n2 , "n2" , 4 , 1 )
    apifun_checkloweq ( "dbldbl_tostr" , n1 , "n1" , 3 , 80 )
    apifun_checkloweq ( "dbldbl_tostr" , n2 , "n2" , 4 , 30 )
    //
    if ( rhs == 1 ) then
      b = dbldbl_ddoutc ( a.dd )
    elseif ( fmt=="e" ) then
      b = dbldbl_ddeform ( a.dd , n1, n2)
    elseif ( fmt=="f" ) then
      b = dbldbl_ddfform ( a.dd , n1, n2)
    end
    b = stripblanks(b)
    b = strcat(b)    
endfunction

