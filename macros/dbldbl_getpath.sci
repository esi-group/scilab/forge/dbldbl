// Copyright (C) 2010 - 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function path = dbldbl_getpath (  )
    // Returns the path to the current module.
    // 
    // Calling Sequence
    //   path = dbldbl_getpath (  )
    //
    // Parameters
    //   path : a 1-by-1 matrix of strings, the path to the current module.
    //
    // Examples
    //   path = dbldbl_getpath (  )
    //
    // Authors
    //   Copyright (C) 2011 - Michael Baudin

    [lhs, rhs] = argn()
    apifun_checkrhs ( "dbldbl_getpath" , rhs , 0:0 )
    apifun_checklhs ( "dbldbl_getpath" , lhs , 1:1 )
    
    path = get_function_path("dbldbl_getpath")
    path = fullpath(fullfile(fileparts(path),".."))
endfunction

