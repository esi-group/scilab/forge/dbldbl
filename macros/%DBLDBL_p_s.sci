// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function b = %DBLDBL_p_s ( a , n )
    // Returns dda^n
    //
    // Calling Sequence
    // ddb = dda^n
    //
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"))

    if ( floor(n) <> n ) then
      b = exp(n*log(a))
    else
      ddb = dbldbl_ddnpwr ( a.dd, n )
      b = dbldbl_new(ddb)
    end
endfunction


