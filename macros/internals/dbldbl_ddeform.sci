// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_ddeform(a, n1, n2)
    //   This routine converts the DD number A to E format, i.e. 1P,En1.n2.
    //   B is the output array (type character*1 of size n1).  N1 must exceed
    //   N2 by at least 8, and N1 must not exceed 80.  N2 must not exceed 30,
    //   i.e., not more than 31 significant digits.
    //
    // Example
    // a=[-2.3;0];
    // b = dbldbl_ddeform(a, 18, 10);
    // b = strcat(b);
    // // b = "-2.2999999999E0   "
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("dbldblinternalslib") then
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"));
    end

    ln = 40

    if (n1 < 0 | n2 < 0 | n1 > 80 | n2 > 30 ..
        | n2 > n1 - 8) then
        error(msprintf("%s: Improper n1, n2 =%d, %d.","dbldbl_ddeform",n1,n2))
    end

    cs = dbldbl_ddoutc (a)
    
    b = blanks(n1);
    
    m1 = n1 - n2 - 8

    for i = 1: m1
        b(i) = ' '
    end

    for i = 1: n2 + 3
        b(i+m1) = cs(i+2)
    end

    for i = 1: 5
        b(i+m1+n2+3) = cs(i+35)
    end
endfunction

