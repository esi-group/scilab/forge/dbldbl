// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ddb = dbldbl_ddnpwr ( a, n )
    //   This computes the N-th power of the DD number A and returns the DD result
    //   in B.  When N is zero, 1 is returned.  When N is negative, the reciprocal
    //   of A ^ |N| is returned. 
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    //   This routine employs the binary method for exponentiation.

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("dbldblinternalslib") then
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"));
    end

    cl2 = 1.4426950408889633d0

    if (a(1) == 0.d0) then
        if (n >= 0) then
            ddb(1) = 0.d0
            ddb(2) = 0.d0
            return
        else
            error(msprintf("%s: Argument is zero and N is negative or zero.","dbldbl_ddnpwr"))
        end
    end

    nn = abs (n)
    if (nn == 0) then
        ddb(1) = 1.d0
        ddb(2) = 0.d0
        return
    elseif (nn == 1) then
        ddb(1) = a(1)
        ddb(2) = a(2)
        ddb = dbldbl_ddnpwrneg ( ddb , n )
        return
    elseif (nn == 2) then
        s2 = dbldbl_ddmuldd (a, a)
        ddb = dbldbl_ddnpwrneg ( s2 , n )
        return
    end

    //   Determine the least integer MN such that 2 ^ MN .GT. NN.

    t1 = nn
    mn = cl2 * log (t1) + 1.d0 + 1.d-14
    s0(1) = a(1)
    s0(2) = a(2)
    s2(1) = 1.d0
    s2(2) = 0.d0
    kn = nn

    //   Compute B ^ N using the binary rule for exponentiation.

    for j = 1: mn
        kk = floor(kn / 2)
        if (kn <> 2 * kk) then
            s1 = dbldbl_ddmuldd (s2, s0)
            s2(1) = s1(1)
            s2(2) = s1(2)
        end
        kn = kk
        if (j < mn) then
            s1 = dbldbl_ddmuldd (s0, s0)
            s0(1) = s1(1)
            s0(2) = s1(2)
        end
    end

    //   Compute reciprocal if N is negative.

    // 110  continue

    s2 = dbldbl_ddnpwrneg ( s2 , n )

    // 120  continue

    ddb(1) = s2(1)
    ddb(2) = s2(2)
endfunction

function ddb = dbldbl_ddnpwrneg ( dda , n )
    //   Compute reciprocal if N is negative.
    if (n < 0) then
        s1(1) = 1.d0
        s1(2) = 0.d0
        ddb = dbldbl_dddivdd (s1, dda)
    else
        ddb = dda
    end
endfunction

