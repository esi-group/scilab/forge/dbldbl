// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ddc = dbldbl_dmuld (da , db )
//   This subroutine computes ddc = da x db.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin


split = 134217729


//>
//   On systems with a fused multiply add, such as IBM systems, it is faster to
//   uncomment the next two lines and comment out the following lines until //>.
//   On other systems, do the opposite.

// s1 = da * db
// s2 = da * db - s1

//   This splits da and db into high-order and low-order words.

cona = da * split
conb = db * split
a1 = cona - (cona - da)
b1 = conb - (conb - db)
a2 = da - a1
b2 = db - b1

//   Multiply da * db using Dekker's method.

s1 = da * db
s2 = (((a1 * b1 - s1) + a1 * b2) + a2 * b1) + a2 * b2
//>
ddc(1) = s1
ddc(2) = s2
endfunction

