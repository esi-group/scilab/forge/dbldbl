// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ddc = dbldbl_dddivd (dda , db )
    // Compute double-double = double-double / double (ddc = dda / db),
    // using a long division scheme. */
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    split = 2^27 + 1;
    // Compute a DP approximation to the quotient. */
    t1 = dda(1) / db;
    // Split t1 and b into two parts with at most 26 bits each,
    // using the Dekker-Veltkamp method. */
    con = t1 * split;
    t11 = con - (con - t1);
    t21 = t1 - t11;
    con = db * split;
    b1 = con - (con - db);
    b2 = db - b1;
    // Compute t1 * b using Dekker’s method. */
    t12 = t1 *  db;
    t22 = (((t11 * b1 - t12) + t11 * b2) + t21 * b1) + t21 * b2;
    // Compute dda - (t12, t22) using Knuth trick. */
    t11 = dda(1) - t12;
    e = t11 - dda(1);
    t21 = ((-t12 - e) + (dda(1) - (t11 - e))) + dda(2) - t22;
    // Compute high-order word of (t11, t21) and divide by b. */
    t2 = (t11 + t21) / db;
    // Renormalize (t1, t2). */
    ddc(1) = t1 + t2;
    ddc(2) = t2 - (ddc(1) - t1);
endfunction

