// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ic = dbldbl_ddcpr ( a, b )
//   This routine compares the DD numbers A and B and returns in IC the value
//   -1, 0, or 1 depending on whether A < B, A = B, or A > B.  It is faster
//   than merely subtracting A and B and looking at the sign of the result.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

if (a(1) < b(1)) then
  ic = -1
elseif (a(1) == b(1)) then
  if (a(2) < b(2)) then
    ic = -1
  elseif (a(2) == b(2)) then
    ic = 0
  else
    ic = 1
  end
else
  ic = 1
end
endfunction

