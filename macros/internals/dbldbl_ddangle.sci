// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function a = dbldbl_ddangle ( x, y )
    //   This computes the DD angle A subtended by the DD pair (X, Y) considered as
    //   a point in the x-y plane.  This is more useful than an arctan or arcsin
    //   routine, since it places the result correctly in the full circle, i.e.
    //   -Pi < A <= Pi.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin


    //   The Taylor series for Sin converges much more slowly than that of Arcsin.
    //   Thus this routine does not employ Taylor series, but instead computes
    //   Arccos or Arcsin by solving Cos (a) = x or Sin (a) = y using one of the
    //   following Newton iterations, both of which converge to a:

    //           z_{k+1} = z_k - [x - Cos (z_k)] / Sin (z_k)
    //           z_{k+1} = z_k + [y - Sin (z_k)] / Cos (z_k)

    //   The first is selected if Abs (x) <= Abs (y); otherwise the second is used.

    //>
    //   Uncomment one of the following two lines, preferably the first if it is
    //   accepted by the compiler.

    pi(1) = 3.1415926535897931D+00
    pi(2) = 1.2246467991473532D-16

    //   Check if both X and Y are zero.

    if (x(1) == 0 & y(1) == 0) then
        error(msprintf("%s: Both arguments are zero.","dbldbl_ddangle"))
    end

    //   Check if one of X or Y is zero.

    if (x(1) == 0) then
        if (y(1) > 0) then
            a = dbldbl_ddmuld (pi, 0.5)
        else
            a = dbldbl_ddmuld (pi, -0.5)
        end
        return
    elseif (y(1) == 0) then
        if (x(1) > 0) then
            a(1) = 0
            a(2) = 0
        else
            a(1) = pi(1)
            a(2) = pi(2)
        end
        return
    end

    //   Normalize x and y so that x^2 + y^2 = 1.

    s0 = dbldbl_ddmuldd (x, x )
    s1 = dbldbl_ddmuldd (y, y )
    s2 = dbldbl_ddadd (s0, s1 )
    s3 = dbldbl_ddsqrt (s2 )
    s1 = dbldbl_dddivdd (x, s3 )
    s2 = dbldbl_dddivdd (y, s3 )

    //   Compute initial approximation of the angle.

    t1 = dbldbl_ddqdc (s1)
    t2 = dbldbl_ddqdc (s2)
    t3 = atan (t2, t1)
    a(1) = t3
    a(2) = 0

    //   The smaller of x or y will be used from now on to measure convergence.
    //   This selects the Newton iteration (of the two listed above) that has the
    //   largest denominator.

    if (abs (t1) <= abs (t2)) then
        kk = 1
        s0(1) = s1(1)
        s0(2) = s1(2)
    else
        kk = 2
        s0(1) = s2(1)
        s0(2) = s2(2)
    end

    //   Perform the Newton-Raphson iteration described.

    for k = 1: 3
        [s1,s2] = dbldbl_ddcssn (a)
        if (kk == 1) then
            s3 = dbldbl_ddsub (s0, s1)
            s4 = dbldbl_dddivdd (s3, s2)
            s1 = dbldbl_ddsub (a, s4)
        else
            s3 = dbldbl_ddsub (s0, s2 )
            s4 = dbldbl_dddivdd (s3, s1 )
            s1 = dbldbl_ddadd (a, s4 )
        end
        a(1) = s1(1)
        a(2) = s1(2)
    end

endfunction

