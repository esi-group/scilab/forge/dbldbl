// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_ddoutc ( a )
    //   Converts the DD number A into character form in the string array B(1:40).
    //   The format is analogous to the Fortran E format.
    //
    // Example
    // a=[-2.3;0];
    // b = dbldbl_ddoutc ( a );
    // b = strcat(b);
    // // b = "  -2.299999999999999822364316059975E0   "
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin


    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("dbldblinternalslib") then
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"));
    end

    //   This routine is called by DDOUT, but it may be directly called by the user
    //   if desired for custom output.

    ln = 40
    digits = "0123456789"
    digits = strsplit(digits,"")

    f = [10; 0]

    ib = zeros(1,ln)

    //   Determine exact power of ten for exponent.

    if (a(1) <> 0) then
        t1 = log10 (abs (a(1)))
        if (t1 >= 0) then
            nx = int(t1)
        else
            nx = int(t1 - 1)
        end
        s0 = dbldbl_ddnpwr (f, nx)
        s1 = dbldbl_dddivdd (a, s0)
        if (s1(1) < 0) then
            s1 = - s1
        end

        //   If we didn't quite get it exactly right, multiply or divide by 10 to fix.
        
        i = 0
        // 100 continue
        while ( %t )

            i = i + 1
            if (s1(1) < 1) then
                nx = nx - 1
                s0 = dbldbl_ddmuld (s1, 10)
                s1 = s0
                if (i <= 3) then
                    // goto 100
                    continue
                else
                    break
                end
            elseif (s1(1) >= 10) then
                nx = nx + 1
                s0 = dbldbl_dddivd (s1, 10)
                s1 = s0
                // goto 100
                continue
            else
                break
            end
        end
    else
        nx = 0
        s1 = [0; 0]
    end

    //   Compute digits.

    for i = 1: ln - 8
        ii = floor(s1(1))
        ib(i) = ii
        f(1) = ii
        s0 = dbldbl_ddsub (s1, f )
        s1 = dbldbl_ddmuld (s0, 10 )
    end

    //   Fix negative digits.
    for i = ln - 8: -1 : 2
        if (ib(i) < 0) then
            ib(i) = ib(i) + 10
            ib(i-1) = ib(i-1) - 1
        end
    end

    if (ib(1) < 0) then
        error(msprintf("%s: Negative leading digit.","dbldbl_ddoutc"))
    end

    //   Round.

    if (ib(ln-8) >= 5) then
        ib(ln-9) = ib(ln-9) + 1

        for i = ln - 9: -1: 2
            if (ib(i) == 10) then
                ib(i) = 0
                ib(i-1) = ib(i-1) + 1
            end
        end

        if (ib(1) == 10) then
            ib(1) = 1
            nx = nx + 1
        end
    end

    //   Insert digit characters in ib.

    b = blanks(40)
    b(1) = ' '
    b(2) = ' '
    if (a(1) >= 0) then
        b(3) = ' '
    else
        b(3) = '-'
    end
    ii = floor(ib(1))
    b(4) = digits(ii+1)
    b(5) = '.'
    b(ln) = ' '

    for i = 2: ln - 9
        ii = floor(ib(i))
        b(i+4) = digits(ii+1)
    end

    //   Insert exponent.

    // 190  continue

    // write (ca, '(i4)') nx
    ca = dbldbl_dddigout (nx, 4)
    b(ln-4) = 'E'
    ii = 0

    for i = 1: 4
        if (ca(i) <> ' ') then
            ii = ii + 1
            b(ln-4+ii) = ca(i)
        end
    end

    for i = ii + 1: 4
        b(ln-4+i) = ' '
    end
endfunction

