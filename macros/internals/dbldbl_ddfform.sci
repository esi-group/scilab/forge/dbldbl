// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_ddfform(a, n1, n2)
    //   This routine converts the DD number A to F format, i.e. Fn1.n2.
    //   B is the output array (type character*1 of size n1).  N1 must exceed
    //   N2 by at least 3, and N1 must not exceed 80.  N2 must not exceed 30.
    //
    // Example
    // a=[-2.3;0];
    // b = dbldbl_ddfform(a, 18, 10);
    // b = strcat(b);
    // // b = "     -2.2999999999"
    //
    // a=[-2.3e123;0];
    // b = dbldbl_ddfform(a, 18, 10);
    // b = strcat(b);
    // // b = "     -2.2999999999"
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("dbldblinternalslib") then
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"));
    end

    ln = 40

    b = blanks(n1)
    c = blanks(40)
    chr40 = blanks(40)

    if (n1 < 0 | n2 < 0 | n1 > 80 | n2 > 30 ..
        | n1 - n2 < 3) then
        error(msprintf("%s: Improper n1, n2 =","dbldbl_ddfform",n1,n2))
    end

    //   Call ddoutc and extract exponent.

    c = dbldbl_ddoutc (a)
    ix = dbldbl_dddigin (c(ln-3:ln), 4)

    for i = 1: n1
        b(i) = ' '
    end

    if (a(1) >= 0.d0) then
        lls = 0
    else
        lls = 1
    end
    if (ix >= 0 & a(1) <> 0.d0) then
        lz = 0
    else
        lz = 1
    end
    mx = max (ix, 0)

    //   Check for overflow of field length.

    if (lls + lz + mx + n2 + 2 > n1) then
        for i = 1: n1
            b(i) = '*'
        end

        return
    end

    //   Check if a zero should be output.

    if (a(1) == 0. | -ix > n2) then
        for i = 1: n1 - n2 - 2
            b(i) = ' '
        end

        b(n1-n2-1) = '0'
        b(n1-n2) = '.'

        for i = 1: n2
            b(i+n1-n2) = '0'
        end

        return
    end

    //   Process other cases.

    if (a(1) < 0.) then
        b(n1-n2-mx-2) = '-'
    end
    if (ix >= 0) then
        b(n1-n2-ix-1) = c(4:4)
        kx = min (ln - 9, ix)

        for i = 1: kx
            b(i+n1-n2-ix-1) = c(i+5:i+5)
        end

        for i = kx + 1: ix
            b(i+n1-n2-ix-1) = '0'
        end

        b(n1-n2) = '.'
        kx = max (min (ln - 9 - ix, n2), 0)

        for i = 1: kx
            b(i+n1-n2) = c(i+ix+5:i+ix+5)
        end

        for i = kx + 1: n2
            b(i+n1-n2) = '0'
        end
    else
        nx = - ix
        b(n1-n2-1) = '0'
        b(n1-n2) = '.'

        for i = 1: nx - 1
            b(i+n1-n2) = '0'
        end

        b(n1-n2+nx) = c(4:4)
        kx = min (ln - 8, n2 - nx)

        for i = 1: kx
            b(i+n1-n2+nx) = c(i+5:i+5)
        end

        for i = kx + 1: n2 - nx
            b(i+n1-n2+nx) = '0'
        end
    end
endfunction

