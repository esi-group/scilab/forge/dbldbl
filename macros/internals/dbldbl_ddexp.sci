// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_ddexp ( a )
    //   This computes the DD angle A subtended by the DD pair (X, Y) considered as
    //   a point in the x-y plane.  This is more useful than an arctan or arcsin
    //   routine, since it places the result correctly in the full circle, i.e.
    //   -Pi < A <= Pi.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin


    //   This computes the exponential function of the DD number A and returns the
    //   DD result in B.

    //   This routine uses a modification of the Taylor's series for Exp (t):

    //   Exp (t) =  (1 + r + r^2 / 2// + r^3 / 3// + r^4 / 4// ...) ^ q * 2 ^ n

    //   where q = 64, r = t' / q, t' = t - n Log(2) and where n is chosen so
    //   that -0.5 Log(2) < t' <= 0.5 Log(2).  Reducing t mod Log(2) and
    //   dividing by 64 insures that -0.004 < r <= 0.004, which accelerates
    //   convergence in the above series.


    nq = 6

    //>
    //   Uncomment one of the following two lines, preferably the first if it is
    //   accepted by the compiler.

    //data al2 / z'3FE62E42FEFA39EF',  z'3C7ABC9E3B39803F'/
    al2 = [6.9314718055994529D-01;  2.3190468138462996D-17]

    //   Check for overflows and underflows.

    if (abs (a(1)) >= 709) then
        if (a(1) > 0) then
            error(msprintf("%s: Argument is too large.","dbldbl_ddexp"))
        else
            b = dbldbl_dddqc (0)
            return
        end
    end

    f(1) = 1
    f(2) = 0

    //   Compute the reduced argument A' = A - Log(2) * Nint [A / Log(2)].  Save
    //   NZ = Nint [A / Log(2)] for correcting the exponent of the final result.

    s0 = dbldbl_dddivdd (a, al2 )
    s1 = dbldbl_ddnint (s0 )
    t1 = s1(1)
    nz = round(t1 + dbldbl_copysign (1.d-14, t1))
    s2 = dbldbl_ddmuldd (al2, s1 )
    s0 = dbldbl_ddsub (a, s2 )

    //   Check if the reduced argument is zero.

    if (s0(1) == 0) then
        s0(1) = 1
        s0(2) = 0
        l1 = 0
    else

        //   Divide the reduced argument by 2 ^ NQ.

        s1 = dbldbl_dddivd (s0, 2 ** nq )

        //   Compute Exp using the usual Taylor series.

        s2(1) = 1
        s2(2) = 0
        s3(1) = 1
        s3(2) = 0
        l1 = 0

        while ( %t )
            l1 = l1 + 1
            if (l1 == 100) then
                error(msprintf("%s: Iteration limit exceeded.","dbldbl_ddexp"))
            end

            t2 = l1
            s0 = dbldbl_ddmuldd (s2, s1 )
            s2 = dbldbl_dddivd (s0, t2 )
            s0 = dbldbl_ddadd (s3, s2 )
            s3 = s0

            //   Check for convergence of the series.

            if (abs (s2(1)) <= 1.d-33 * abs (s3(1))) then
                break
            end
        end

        //   Raise to the (2 ^ NQ)-th power.

        for i = 1: nq
            s0 = dbldbl_ddmuldd (s0, s0)
        end
    end

    //  Multiply by 2 ^ NZ.
    b = dbldbl_ddmuld (s0, 2.d0 ** nz)
    //   Restore original precision level.
endfunction

function r = dbldbl_copysign(x,y)
    // Returns r with the magnitude of x and the sign of y.
    //
    // Calling Sequence
    // r = dbldbl_copysign(x,y)
    //
    // Parameters
    // x : a matrix of complex doubles
    // y : a matrix of complex doubles
    // r : a matrix of complex doubles, the magnitude of x and the sign of y.
    //
    // Description
    // If y is positive or negative zero, the sign of r is positive.
    //
    // Examples
    //    x = [-5 -4 0 4 5]
    //    y = [-1 0 1 2 3]
    //    r = dbldbl_copysign(x,y)
    //    expected = [-5 4 0 4 5]
    //
    // Bibliography
    //  ISO/IEC 9899 - Programming languages - C, http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1256.pdf
    //
    // Authors
    // Copyright (C) 2010 - Michael Baudin

    [lhs,rhs]=argn()
    if ( rhs<>2 ) then
        errmsg = sprintf(gettext("%s: Unexpected number of arguments : %d provided while %d to %d are expected."),..
        "dbldbl_copysign",rhs,1,1)
        error(errmsg)
    end
    if ( size(x) <> size(y) ) then
        errmsg = sprintf(gettext("%s: Size of x and y are different."),..
        "dbldbl_copysign")
        error(errmsg)
    end
    r = zeros(x)
    k = find(y<>0)
    r(k) = abs(x(k)).*sign(y(k))
    k = find(y==0)
    r(k) = abs(x(k))
endfunction

