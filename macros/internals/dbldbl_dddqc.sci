// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_dddqc ( a )
//   This routine converts the DP number A to DD form in B.  All bits of
//   A are recovered in B.  However, note for example that if A = 0.1D0 and N
//   is 0, then B will NOT be the DD equivalent of 1/10.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    b(1) = a
    b(2) = 0
    
endfunction

