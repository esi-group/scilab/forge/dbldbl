// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_ddinpc ( a )
    //   Converts the CHARACTER*80 array A into the DD number B.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 foruble-foruble precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("dbldblinternalslib") then
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"));
    end

    lna = length(a)
    if (lna > 80) then
        error(msprintf(gettext("%s: String too long.\n"),"dbldbl_ddinpc"))
    end
    lnblanks = 80 - lna
    a = a+blanks(lnblanks)
    a = strsplit(a,"")

    ln = 80
    dig = "0123456789"

    id = 0
    ip = -1
    is = 0
    inz = 0
    s1(1) = 0
    s1(2) = 0

    for i = 80:-1: 1
        if (a(i) <> ' ') then
            break
        end
    end

    lnn = i

    //   Scan for digits, looking for the period also.

    for i = 1: lnn
        ai = a(i)
        if (ai == ' ' & id == 0) then
        elseif (ai == '.') then
            if (ip >= 0) then
                dbldbl_inpcerror()
            end
            ip = id
            inz = 1
        elseif (ai == '+') then
            if (id <> 0 | ip >= 0 | is <> 0) then
                dbldbl_inpcerror()
            end
            is = 1
        elseif (ai == '-') then
            if (id <> 0 | ip >= 0 | is <> 0) then
                dbldbl_inpcerror()
            end
            is = -1
        elseif (ai == 'e' | ai == 'E' | ai == 'd' | ai == 'D') then
            break
        else
            bi = strindex (dig, ai)
            if (bi==[]) then
                dbldbl_inpcerror()
            else
                bi = bi(1) - 1
                if (inz > 0 | bi > 0) then
                    inz = 1
                    id = id + 1
                    s0 = dbldbl_ddmuld (s1, 10 )
                    f(1) = bi
                    f(2) = 0
                    f = dbldbl_dddqc (bi )
                    s1 = dbldbl_ddadd (s0, f )
                end
            end
        end
    end

    if (is == -1) then
        s1(1) = - s1(1)
        s1(2) = - s1(2)
    end
    k = i
    if (ip == -1) then
        ip = id
    end
    ie = 0
    is = 0
    ca = strsplit(blanks(10),"")

    for i = k + 1: lnn
        ai = a(i)
        if (ai == ' ') then
        elseif (ai == '+') then
            if (ie <> 0 | is <> 0) then
                dbldbl_inpcerror()
            end
            is = 1
        elseif (ai == '-') then
            if (ie <> 0 | is <> 0) then
                dbldbl_inpcerror()
            end
            is = -1
        elseif (strindex (dig, ai) == []) then
            dbldbl_inpcerror()
        else
            ie = ie + 1
            if (ie > 3) then
                dbldbl_inpcerror()
            end
            ca(ie) = ai
        end
    end

    // read (ca, '(i4)') ie
    ie = dbldbl_dddigin (ca, 4)
    if (is == -1) then
        ie = - ie
    end
    ie = ie + ip - id
    s0(1) = 10
    s0(2) = 0
    s2 = dbldbl_ddnpwr (s0, ie )
    b = dbldbl_ddmuldd (s1, s2 )
endfunction

function dbldbl_inpcerror()
    error(msprintf(gettext("%s: Syntax error in literal string.\n"),"dbldbl_ddinpc"))
endfunction


