// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ddc = dbldbl_dddivdd (dda , ddb )
    //   This divides the DD number DDA by the DD number DDB to yield the DD
    //   quotient DDC.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    split = 134217729

    //   Compute a DP approximation to the quotient.

    s1 = dda(1) / ddb(1)
    //>
    //   On systems with a fused multiply add, such as IBM systems, it is faster to
    //   uncomment the next two lines and comment out the following lines until //>.
    //   On other systems, do the opposite.

    // c11 = s1 * ddb(1)
    // c21 = s1 * ddb(1) - c11

    //   This splits s1 and ddb(1) into high-order and low-order words.

    cona = s1 * split
    conb = ddb(1) * split
    a1 = cona - (cona - s1)
    b1 = conb - (conb - ddb(1))
    a2 = s1 - a1
    b2 = ddb(1) - b1

    //   Multiply s1 * ddb(1) using Dekker's method.

    c11 = s1 * ddb(1)
    c21 = (((a1 * b1 - c11) + a1 * b2) + a2 * b1) + a2 * b2
    //>
    //   Compute s1 * ddb(2) (only high-order word is needed).

    c2 = s1 * ddb(2)

    //   Compute (c11, c21) + c2 using Knuth's trick.

    t1 = c11 + c2
    e = t1 - c11
    t2 = ((c2 - e) + (c11 - (t1 - e))) + c21

    //   The result is t1 + t2, after normalization.

    t12 = t1 + t2
    t22 = t2 - (t12 - t1)

    //   Compute dda - (t12, t22) using Knuth's trick.

    t11 = dda(1) - t12
    e = t11 - dda(1)
    t21 = ((-t12 - e) + (dda(1) - (t11 - e))) + dda(2) - t22

    //   Compute high-order word of (t11, t21) and divide by ddb(1).

    s2 = (t11 + t21) / ddb(1)

    //   The result is s1 + s2, after normalization.

    ddc(1) = s1 + s2
    ddc(2) = s2 - (ddc(1) - s1)
endfunction

