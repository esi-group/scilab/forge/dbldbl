// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_ddnint ( a )
    //   This sets B equal to the integer nearest to the DD number A.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin


    t105 = 2.d0 ** 105
    t52 = 2.d0 ** 52
    con  = [t105; t52]

    //   Check if  A  is zero.

    if (a(1) == 0)  then
        b(1) = 0
        b(2) = 0
        return
    end

    if (a(1) >= t105) then
        error(msprintf("%s: Argument is too large.","dbldbl_ddnint"))
    end

    if (a(1) > 0) then
        s0 = dbldbl_ddadd (a, con )
        b = dbldbl_ddsub (s0, con )
    else
        s0 = dbldbl_ddsub (a, con )
        b = dbldbl_ddadd (s0, con )
    end
endfunction

