// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function b = dbldbl_ddlog ( a )
    //   This computes the natural logarithm of the DD number A and returns the DD
    //   result in B.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin



    //   The Taylor series for Log converges much more slowly than that of Exp.
    //   Thus this routine does not employ Taylor series, but instead computes
    //   logarithms by solving Exp (b) = a using the following Newton iteration,
    //   which converges to b:

    //           x_{k+1} = x_k + [a - Exp (x_k)] / Exp (x_k)

    //   These iterations are performed with a maximum precision level NW that
    //   is dynamically changed, approximately doubling with each iteration.

    //>
    //   Uncomment one of the following two lines, preferably the first if it is
    //   accepted by the compiler.

    //data al2 / z'3FE62E42FEFA39EF',  z'3C7ABC9E3B39803F'/
    al2 = [6.9314718055994529D-01;  2.3190468138462996D-17]

    if (a(1) <= 0) then
        error(msprintf("%s: Argument is less than or equal to zero.","dbldbl_ddlog"))
    end

    //   Compute initial approximation of Log (A).

    t1 = a(1)
    t2 = log (t1)
    b = [t2; 0]

    //   Perform the Newton-Raphson iteration described above.

    for k = 1: 3
        s0 = dbldbl_ddexp (b )
        s1 = dbldbl_ddsub (a, s0 )
        s2 = dbldbl_dddivdd (s1, s0 )
        s1 = dbldbl_ddadd (b, s2 )
        b = s1
    end

endfunction

