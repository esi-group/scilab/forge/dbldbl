// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ca = dbldbl_dddigout (a, n)
    // ca = dbldbl_dddigout (10, 4)
    // // ca = [" ";" ";"1";"0";" ";" ";" ";" ";" ";" ";" ";" ";" ";" ";" ";" "]
    //
    // ca = dbldbl_dddigout (-10, 4)
    // // ca = [" ";"-";"1";"0";" ";" ";" ";" ";" ";" ";" ";" ";" ";" ";" ";" "]

    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("dbldblinternalslib") then
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"));
    end

    digits = "0123456789"
    digits = strsplit(digits,"")

    ca = blanks(16)
    ca = strsplit(ca)
    
    is = dbldbl_copysign (1.d0, a)
    d1 = abs (a)

    set_i = %t

    for i = n : -1 : 1
        d2 = floor (d1 / 10.d0)
        k = floor(1.d0 + (d1 - 10.d0 * d2))
        d1 = d2
        ca(i) = digits(k)
        if (d1 == 0.d0) then
            set_i = %f
            break
            //goto 100
        end
    end

    if ( set_i ) then
        i = 0
    end

    // 100 continue

    if (is < 0 & i > 1) then
        ca(i-1) = '-'
    elseif (i == 0 | is < 0 & i == 1) then
        ca = ['*' '*' '*' '*' '*' '*' '*' '*' '*' '*' '*' '*' '*' '*' '*' '*']
    end
endfunction

