// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function ddb = dbldbl_ddnrt ( a, n )
    //   This computes the N-th root of the DD number A and returns the DD result
    //   in B.  N must be at least one.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin


    //   This subroutine employs the following Newton-Raphson iteration, which
    //   converges to A ^ (-1/N):

    //    X_{k+1} = X_k + (X_k / N) * (1 - A * X_k^N)

    //   The reciprocal of the final approximation to A ^ (-1/N) is the N-th root.

    if (a(1) == 0 ) then
        b(1) = 0.d0
        b(2) = 0.d0
        return
    elseif (a(1) < 0 ) then
        error (msprintf("%s: Argument is negative.","dbldbl_ddnrt"))
    end
    if (n <= 0) then
        error (msprintf("%s: Improper value of N.","dbldbl_ddnrt"))
    end

    //   Handle cases N = 1 and 2.

    if (n == 1) then
        ddb(1) = a(1)
        ddb(2) = a(1)
        return
    elseif (n == 2) then
        ddb = dbldbl_ddsqrt (a)
        return
    end

    f1(1) = 1
    f1(2) = 0

    //   Compute the initial approximation of A ^ (-1/N).

    tn = n
    t1 = a(1)
    t2 = exp (- log (t1) / tn)
    b(1) = t2
    b(2) = 0.d0

    //   Perform the Newton-Raphson iteration described above.

    for k = 1: 3
        s0 = dbldbl_ddnpwr (b, n)
        s1 = dbldbl_ddmuldd (a, s0)
        s0 = dbldbl_ddsub (f1, s1)
        s1 = dbldbl_ddmuldd (b, s0)
        s0 = dbldbl_dddivd (s1, tn)
        s1 = dbldbl_ddadd (b, s0)
        b(1) = s1(1)
        b(2) = s1(2)
    end

    //   Take the reciprocal to give final result.

    s1 = dbldbl_dddivdd (f1, b)
    ddb(1) = s1(1)
    ddb(2) = s1(2)

endfunction

