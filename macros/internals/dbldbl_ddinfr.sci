// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


function [b,c] = dbldbl_ddinfr ( a )
//   Sets B to the integer part of the DD number A and sets C equal to the
//   fractional part of A.  Note that if A = -3.3, then B = -3 and C = -0.3.
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin


    // Load Internals lib if it's not already loaded
    // =========================================================================
    if ~ exists("dbldblinternalslib") then
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"));
    end

t105 = 2 ** 105
t52 = 2 ** 52
con = [t105; t52]

//   Check if  A  is zero.

if (a(1) == 0)  then
  b(1) = 0
  b(2) = 0
  c(1) = 0
  c(2) = 0
  return
end

if (a(1) >= t105) then
        error (msprintf("%s: Argument is too large.","dbldbl_ddinfr"))
end

f(1) = 1
f(2) = 0
if (a(1) > 0) then
  s0 = dbldbl_ddadd (a, con )
  b = dbldbl_ddsub (s0, con )
  ic = dbldbl_ddcpr (a, b )
  if (ic >= 0) then
    c = dbldbl_ddsub (a, b )
  else
    s1 = dbldbl_ddsub (b, f )
    b(1) = s1(1)
    b(2) = s1(2)
    c = dbldbl_ddsub (a, b )
  end
else
  s0 = dbldbl_ddsub (a, con )
  b = dbldbl_ddadd (s0, con )
  ic = dbldbl_ddcpr (a, b )
  if (ic <= 0) then
    c = dbldbl_ddsub (a, b )
  else
    s1 = dbldbl_ddadd (b, f )
    b(1) = s1(1)
    b(2) = s1(2)
    c = dbldbl_ddsub (a, b )
  end
end
endfunction

