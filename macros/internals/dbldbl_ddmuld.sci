// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function ddc = dbldbl_ddmuld( dda,  db)
    // Compute double-double = double-double * double (ddc = dda * db). */
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    split = 2^27 + 1;
    // Split dda(1) and db into two parts with at most 26 bits each,
    // using the Dekker-Veltkamp method. */
    con = dda(1) * split;
    a11 = con - dda(1);
    a11 = con - a11;
    a21 = dda(1) - a11;
    con = db * split;
    b1 = con - db;
    b1 = con - b1;
    b2 = db - b1;
    // Compute dda(1) * db using Dekker’s method. */
    c11 = dda(1) * db;
    c21 = (((a11 * b1 - c11) + a11 * b2) + a21 * b1) + a21 * b2;
    c2 = dda(2) * db;
    // Compute (c11, c21) + c2. */
    t1 = c11 + c2;
    t2 = (c2 - (t1 - c11)) + c21;
    // Renormalize (t1, t2) */
    ddc(1) = t1 + t2;
    ddc(2) = t2 - (ddc(1) - t1);
endfunction

