// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

function z = dbldbl_new ( varargin )
    // Creates a double-double.
    //
    // Calling Sequence
    // a = dbldbl_new()
    // a = dbldbl_new(high)
    // a = dbldbl_new(high,low)
    // a = dbldbl_new(dd)
    // a = dbldbl_new(str)
    //
    // Parameters
    // high : a 1-by-1 matrix of real doubles (no complex), the high-order part (default high=0).
    // low : a 1-by-1 matrix of real doubles (no complex), the low-order part (default low=0). It is expected that abs(low)<abs(high).
    // dd : a 2-by-1 matrix of real doubles (no complex), dd(1) is the high order part, and dd(2) is the low order part. It is expected that abs(dd(2))<abs(dd(1)).
    // a : a double-double, a is the unevaluated sum a.dd(1) + a.dd(2), a.dd(1) is the high order part (most significant digits), and a.dd(2) is the low order part (least significant digits).
    // a.dd : a 2-by-1 matrix of doubles, the highest and lowest part of a.
    // str : a 1-by-1 matrix of strings, with length less than 80, the decimal digits of the double-double.
    //
    // Description
    // Creates a double-double from a double
    //
    // Examples
    // // Create doubles-doubles from double
    // a = dbldbl_new ( )
    // a = dbldbl_new ( 2 )
    // a = dbldbl_new ( 2 , 3.d-20)
    // a = dbldbl_new ( [2 , 3.d-20] )
    // // Create from decimal string
    // a = dbldbl_new ( "1.23456789012345678901234567890" )
    // a = dbldbl_new ( "1.23456789012345678901234567890e123" )
    // a = dbldbl_new ( "-1.23456789012345678901234567890E-123" )
    // a = dbldbl_new ( "1.23456789012345678901234567890d123" )
    // a = dbldbl_new ( "1.23456789012345678901234567890D123" )
    // a = dbldbl_new ( "00000001.23456789012345678901234567890D123" )
    // // Additions, subtractions
    // a = dbldbl_new ( 2 )
    // b = dbldbl_new ( 3 )
    // 1+a
    // a+1
    // a+b
    // -a
    // a-b
    // 1-a
    // // Multiplications, divisions
    // a = dbldbl_new ( 2 )
    // b = dbldbl_new ( 3 )
    // 3*b
    // a*3
    // a*b
    // a/b
    // a/3
    // 3/a
    // // Powers, nth-root
    // a = dbldbl_new ( 2 )
    // a^2
    // a^-3
    // a^1.5
    // b = dbldbl_new ( 3 )
    // a^b
    // b = dbldbl_new ( 1.5 )
    // a^b
    // // Nth-root (see also dbldbl_nthroot)
    // sqrt(a)
    // // Compare: ==, <, >, <=, >=
    // a = dbldbl_new ( 2 );
    // b = dbldbl_new ( 3 );
    // a == b
    // a > b
    // a >= b
    // a < b
    // a <= b
    // // Compare dd op d, d op dd, with op: <, <=, >, >=
    // a = dbldbl_new ( 2 );
    // a < 1
    // a <= 1
    // a > 1
    // a >= 1
    // 1 < a
    // 1 <= a
    // 1 > a
    // 1 >= a
    // // Round to nearest integer
    // a = dbldbl_new ( 2.3);
    // round(a)
    // a = dbldbl_new ( 2.7);
    // round(a)
    // a = dbldbl_new ( -2.3);
    // round(a)
    // a = dbldbl_new ( -2.7);
    // round(a)
    // // Round to zero
    // a = dbldbl_new ( 2.3);
    // int(a)
    // a = dbldbl_new ( 2.7 );
    // int(a)
    // a = dbldbl_new ( -2.7 );
    // int(a)
    // a = dbldbl_new ( -2.3 );
    // int(a)
    // // cos, sin, tan
    // a = dbldbl_new ( 2 )
    // cos(a)
    // sin(a)
    // tan(a)
    // // exp, log
    // a = dbldbl_new ( 2 )
    // exp(a)
    // log(a)
    // // acos, asin, atan(x), atan(y,x)
    // x = dbldbl_new ( 0.75);
    // acos(x)
    // asin(x)
    // atan(x)
    // y = dbldbl_new ( 3 );
    // atan(y,x)
    // // abs
    // a = dbldbl_new ( 2.3 );
    // abs(a)
    // a = dbldbl_new ( -2.3 );
    // abs(a)
    //
    // Bibliography
    //  "Design, Implementation and Testing of Extended and Mixed Precision BLAS", Li et al, 2000
    //  "A Fortran-90 double-double precision library", David Bailey, http://www.nersc.gov/∼dhbailey/mpdist/mpdist.html.
    //
    // Authors
    // Copyright (C) 2011 - Michael Baudin

    function argin = argindefault ( rhs , vararglist , ivar , default )
        // Returns the value of the input argument #ivar.
        // If this argument was not provided, or was equal to the 
        // empty matrix, returns the default value.
        if ( rhs < ivar ) then
            argin = default
        else
            if ( vararglist(ivar) <> [] ) then
                argin = vararglist(ivar)
            else
                argin = default
            end
        end
    endfunction

    // Load Internals lib
        path = dbldbl_getpath (  )
        dbldblinternallib  = lib(fullfile(path,"macros","internals"))

    [lhs,rhs]=argn()
    apifun_checkrhs ( "dbldbl_new" , rhs , 0:2 )
    apifun_checklhs ( "dbldbl_new" , lhs , 0:1 )
    //
    high = argindefault ( rhs , varargin , 1 , 0 )
    low = argindefault ( rhs , varargin , 2 , 0 )
    //
    // Check type
    apifun_checktype ( "dbldbl_new" , high , "high" , 1 , ["constant" "string"])
    apifun_checktype ( "dbldbl_new" , low , "low" , 2 , "constant" )
    //
    // Check size
    argsize = size(high,"*")
    if ( argsize > 2 ) then
        error(msprintf(gettext("%s: Wrong size for input argument #%d: %d-by-%d matrix expected.\n"),"dbldbl_new",2,1))
    end 
    apifun_checkscalar ( "dbldbl_new" , low , "low" , 2 )
    //
    // Check arguments consistency
    if ( argsize == 2 ) then
      if ( rhs == 2 ) then
        error(msprintf(gettext("%s: Wrong number of input argument: %d expected.\n"),"dbldbl_new",1))
      end
    end
    hightype = typeof(high)
    if ( argsize == 2 ) then
      if ( hightype == "string" ) then
        error(msprintf(gettext("%s: Wrong type of input argument.\n"),"dbldbl_new"))
      end
    end
    //
    if ( hightype == "string" ) then
        // a = dbldbl_new(str)
        z = dbldbl_newstring(high)
    else
      if ( rhs == 2 ) then
        // a = dbldbl_new(high,low)
        z = dbldbl_newtwoargs(high,low)
      elseif ( argsize == 2 ) then
        // a = dbldbl_new(dd)
        z = dbldbl_newdd(high)
      else
        // a = dbldbl_new()
        // a = dbldbl_new(high)
        z = dbldbl_newtwoargs(high,low)
      end
    end
endfunction
function z = dbldbl_newempty()
    z = tlist([
    "DBLDBL"
    "dd"
    ])
endfunction
function z = dbldbl_newtwoargs(high,low)
    z = dbldbl_newempty()
    //
    if ( abs(high) < abs(low) ) then
        error(msprintf(gettext("%s: Wrong magnitude of arguments.\n"),"dbldbl_new"))
    end
    //
    z.dd(1) = high
    z.dd(2) = low
endfunction
function z = dbldbl_newstring(str)
    z = dbldbl_newempty()
    //
    b = dbldbl_ddinpc ( str )
    //
    z.dd = b
endfunction
function z = dbldbl_newdd(dd)
    z = dbldbl_newempty()
    //
    if ( abs(dd(1)) < abs(dd(2)) ) then
        error(msprintf(gettext("%s: Wrong magnitude of arguments.\n"),"dbldbl_new"))
    end
    //
    z.dd = dd
endfunction

