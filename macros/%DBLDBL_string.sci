// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt





//
// %DBLDBL_string --
//   Returns the string containing the double-double
//
function str = %DBLDBL_string ( this )
  str = []
  k = 1
  str(k) = msprintf("Double-double:")
  k = k + 1
  str(k) = msprintf("==============")
  k = k + 1
  str(k) = msprintf("%s\n", dbldbl_tostr(this))
  k = k + 1
endfunction


