// Copyright (C) 2011 - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt


// <-- CLI SHELL MODE -->
// <-- ENGLISH IMPOSED -->
//
// new
a = dbldbl_new ( );
assert_checkequal(a.dd(1),0);
assert_checkequal(a.dd(2),0);
//
a = dbldbl_new ( 2 );
assert_checkequal(a.dd(1),2);
assert_checkequal(a.dd(2),0);
//
a = dbldbl_new ( 2 , 1.d-20 );
assert_checkequal(a.dd(1),2);
assert_checkequal(a.dd(2),1.d-20);
//
a = dbldbl_new ( [2 , 3.d-20] );
assert_checkequal(a.dd(1),2);
assert_checkequal(a.dd(2),3.d-20);
//
a = dbldbl_new ( "1.23456789012345678901234567890" );
assert_checkequal(a.dd(1),1.2345678901234567);
assert_checkequal(a.dd(2),9.85802102047885653E-017);
//
a = dbldbl_new ( "1.23456789012345678901234567890E123" );
assert_checkequal(a.dd(1),1.23456789012345681E+123 );
assert_checkequal(a.dd(2),-2.54412302223302791E+106);
//
a = dbldbl_new ( "-1.23456789012345678901234567890E-123" );
assert_checkequal(a.dd(1),-1.23456789012345684E-123 );
assert_checkequal(a.dd(2),4.62051539247579440E-140);
//
a = dbldbl_new ( "1.23456789012345678901234567890d123" );
assert_checkequal(a.dd(1),1.23456789012345681E+123 );
assert_checkequal(a.dd(2),-2.54412302223302791E+106);
//
a = dbldbl_new ( "1.23456789012345678901234567890D123" );
assert_checkequal(a.dd(1),1.23456789012345681E+123 );
assert_checkequal(a.dd(2),-2.54412302223302791E+106);
//
a = dbldbl_new ( "00000001.23456789012345678901234567890D123" );
assert_checkequal(a.dd(1),1.23456789012345681E+123 );
assert_checkequal(a.dd(2),-2.54412302223302791E+106);
//
//
// Addition
//
a = dbldbl_new ( 3 , 4.d-20);
b = dbldbl_new ( 2 , 5.d-20);
c = a + b;
assert_checkequal(c.dd(1),5);
assert_checkequal(c.dd(2),9.d-20);
//
b = dbldbl_new ( 2 , 5.d-20);
c = 1 + b;
assert_checkequal(c.dd(1),3);
assert_checkequal(c.dd(2),5.d-20);
//
b = dbldbl_new ( 2 , 5.d-20);
c = b + 2;
assert_checkequal(c.dd(1),4);
assert_checkequal(c.dd(2),5.d-20);
//
// A test which fails with doubles, 
// because 1+%eps/2==1.
//
a = dbldbl_new ( 1 );
b = dbldbl_new ( %eps/2 );
c = a + b;
assert_checkequal(c.dd(1),1);
assert_checkequal(c.dd(2),%eps/2);
//
// Multiplication
//
b = dbldbl_new ( 2 , 5.d-20);
c = b * 3;
assert_checkequal(c.dd(1),6);
assert_checkequal(c.dd(2),15.d-20);
//
a = dbldbl_new ( 2 , 5.d-20);
c = 3 * a;
assert_checkequal(c.dd(1),6);
assert_checkequal(c.dd(2),15.d-20);
//
a = dbldbl_new ( 2 , 4.d-20);
b = dbldbl_new ( 3 , 5.d-20);
c = a * b;
assert_checkequal(c.dd(1),6);
assert_checkequal(c.dd(2),2.199999999999999849e-19);
//
// A real case of accuracy testing
// The exact difference: a*a - e = 4.64...e-35
// a is the double-double representation of 1.23456789123456.
a = dbldbl_new(5559999494927543 * 2^-52 , 8123412135034948 * 2^-106);
b = a * a;
e = dbldbl_new ( 6864196851717916 * 2^-52 , -5607781580708837 * 2^-113);
rtol = dbldbl_new(1.e-32);
assert_checktrue((b-e)/e < rtol);
//
// Division
//
a = dbldbl_new ( 2 , 5.d-20);
c = a / 3;
assert_checkequal(c.dd(1),2/3);
assert_checkequal(c.dd(2),3.702410082083854997D-17);
//
a = dbldbl_new ( 2 , 5.d-20);
c = 3 / a;
assert_checkequal(c.dd(1),1.5);
assert_checkequal(c.dd(2),-3.749999999999999907D-20);
//
a = dbldbl_new ( 6 , 4.d-20);
b = dbldbl_new ( 3 , 5.d-20);
c = a / b;
assert_checkequal(c.dd(1),2);
assert_checkequal(c.dd(2),-2.e-20);
//
// Subtraction
//
a = dbldbl_new ( 2 , 5.d-20);
c = -a;
assert_checkequal(c.dd(1),-2);
assert_checkequal(c.dd(2),-5.d-20);
//
a = dbldbl_new ( 2 , 4.d-20);
b = dbldbl_new ( 3 , 5.d-20);
c = a-b;
assert_checkequal(c.dd(1),-1);
assert_checkequal(c.dd(2),-1.000000000000000096D-20);
//
x = dbldbl_new(3);
c = 1-x;
assert_checkequal(c.dd(1),-2);
assert_checkequal(c.dd(2),0);
//
x = dbldbl_new(3);
c = x-1;
assert_checkequal(c.dd(1),2);
assert_checkequal(c.dd(2),0);
//
// Pi
//
ddpi = dbldbl_pi ( );
assert_checkequal(ddpi.dd(1),%pi);
//
// Power: dd^d
//
a = dbldbl_new ( 3 );
c = a^1;
assert_checkequal(c.dd(1),3);
//
c = a^-1;
assert_checkequal(c.dd(1),3^-1);
//
c = a^2;
assert_checkequal(c.dd(1),3^2);
//
c = a^-2;
assert_checkequal(c.dd(1),3^-2);
//
c = a^3;
assert_checkequal(c.dd(1),3^3);
//
c = a^-3;
assert_checkequal(c.dd(1),3^-3);
//
c = a^4;
assert_checkequal(c.dd(1),3^4);
//
c = a^-4;
assert_checkequal(c.dd(1),3^-4);
//
c = a^100;
assert_checkequal(c.dd(1),5.15377520732011331036461129765621272702107522001e47);
//
c = a^-100;
assert_checkequal(c.dd(1),1.940325217482632837588506028804650381214116686498101e-48);
//
// Case where d is not integer
c = a^1.5;
assert_checkequal(c.dd(1),5.196152422706631880582339024517);
//
// Power: dd^dd
//
a = dbldbl_new ( 3 );
b = dbldbl_new ( 1.5 );
c = a^b;
assert_checkequal(c.dd(1),5.196152422706631880582339024517);
//
a = dbldbl_new ( 3 );
b = dbldbl_new ( 2 );
c = a^b;
assert_checkequal(c.dd(1),9);
assert_checkequal(c.dd(2),0);
//
// Square root
//
a = dbldbl_new ( 3 );
c = sqrt(a);
assert_checkequal(c.dd(1),sqrt(3));
//
// N-th root
x = dbldbl_new ( 3 );
y = dbldbl_nthroot ( x , 4 );
assert_checkequal(y.dd(1),1.3160740129524924608192189017969990551600685902058221767319);
//
// Compare: ==, <, >, <=, >=
//
a = dbldbl_new ( 2 , 4.d-20);
b = dbldbl_new ( 3 , 5.d-20);
c = and(a == b);
assert_checkfalse(c);
//
a = dbldbl_new ( 2 , 4.d-20);
b = dbldbl_new ( 2 , 4.d-20);
c = (a == b);
assert_checktrue(c);
//
a = dbldbl_new ( 2 );
b = dbldbl_new ( 3 );
c = (a < b);
assert_checktrue(c);
//
a = dbldbl_new ( 2 );
b = dbldbl_new ( 3 );
c = (a <= b);
assert_checktrue(c);
//
a = dbldbl_new ( 2 );
b = dbldbl_new ( 3 );
c = (a > b);
assert_checkfalse(c);
//
a = dbldbl_new ( 2 );
b = dbldbl_new ( 3 );
c = (a >= b);
assert_checkfalse(c);
//
a = dbldbl_new ( 2 );
b = dbldbl_new ( 2 );
c = (a >= b);
assert_checktrue(c);
//
a = dbldbl_new ( 2 );
b = dbldbl_new ( 2 );
c = (a <= b);
assert_checktrue(c);
//
// Compare: d op dd, op : <, <=, >, >=
//
a = dbldbl_new ( 2 );
c = (1 < a);
assert_checktrue(c);
//
a = dbldbl_new ( 2 );
c = (1 <= a);
assert_checktrue(c);
//
a = dbldbl_new ( 2 );
c = (1 > a);
assert_checkfalse(c);
//
a = dbldbl_new ( 2 );
c = (1 >= a);
assert_checkfalse(c);
//
// Compare: dd op d, op : <, <=, >, >=
//
a = dbldbl_new ( 2 );
c = (a < 1);
assert_checkfalse(c);
//
a = dbldbl_new ( 2 );
c = (a <= 1);
assert_checkfalse(c);
//
a = dbldbl_new ( 2 );
c = (a > 1);
assert_checktrue(c);
//
a = dbldbl_new ( 2 );
c = (a >= 1);
assert_checktrue(c);
//
// Round to nearest integer
//
a = dbldbl_new ( 2.3 );
b = round(a);
assert_checkequal(b.dd(1),2);
assert_checkequal(b.dd(2),0);
//
a = dbldbl_new ( 2.7 );
b = round(a);
assert_checkequal(b.dd(1),3);
assert_checkequal(b.dd(2),0);
//
a = dbldbl_new ( -2.7 );
b = round(a);
assert_checkequal(b.dd(1),-3);
assert_checkequal(b.dd(2),0);
//
a = dbldbl_new ( -2.3 );
b = round(a);
assert_checkequal(b.dd(1),-2);
assert_checkequal(b.dd(2),0);
//
// Round to zero
//
a = dbldbl_new ( 2.3);
b = int(a);
assert_checkequal(b.dd(1),2);
assert_checkequal(b.dd(2),0);
//
a = dbldbl_new ( 2.7 );
b = int(a);
assert_checkequal(b.dd(1),2);
assert_checkequal(b.dd(2),0);
//
a = dbldbl_new ( -2.7 );
b = int(a);
assert_checkequal(b.dd(1),-2);
assert_checkequal(b.dd(2),0);
//
a = dbldbl_new ( -2.3 );
b = int(a);
assert_checkequal(b.dd(1),-2);
assert_checkequal(b.dd(2),0);
//
// cos, sin, tan
//
a = dbldbl_new ( 2 , 0);
x = cos(a);
assert_checkequal(x.dd(1),-0.416146836547142386997568229500762189766000771075544890755);
//
a = dbldbl_new ( 2 , 0);
y = sin(a);
assert_checkequal(y.dd(1),0.9092974268256816953960198659117448427022549714478902683789);
//
a = dbldbl_new ( 2 , 0);
z = tan(a);
assert_checkequal(z.dd(1),-2.185039863261518991643306102313682543432017746227663164562);
//
// exp, log
//
a = dbldbl_new ( 3 );
b = exp(a);
assert_checkequal(b.dd(1),20.085536923187667740928529654581717896987907838554150144378);
//
a = dbldbl_new ( 3 );
b = log(a);
assert_checkequal(b.dd(1),1.0986122886681096913952452369225257046474905578227494517346);
//
// Convert to decimal
//
a = dbldbl_new ( 1.23456789123456789 , 1.2345678912345678e-16 );
str = dbldbl_tostr(a);
assert_checkequal(str,"1.234567891234568016933781263224E0");
//
a = dbldbl_new ( 1.23456789123456789e-10 , 1.2345678912345678e-26 );
str = dbldbl_tostr(a);
assert_checkequal(str,"1.234567891234568032781714766793E-10");

//
// Simulate an error while setting the number
//
// Switch high and low: error !
// a = dbldbl_new ( 1.2345678912345678e-26 , 1.23456789123456789e-10);
//
// acos, asin, atan
//
x = dbldbl_new(0.75);
a = acos(x);
assert_checkequal(a.dd(1),0.7227342478134156111783773526413333620252184864244402676267);
//
y = dbldbl_new(0.25);
a = asin(y);
assert_checkequal(a.dd(1),0.2526802551420786534856574369937109722521937330968381936339);
//
y = dbldbl_new(2);
x = dbldbl_new(3);
a = atan(y,x);
assert_checkequal(a.dd(1),0.5880026035475675512456110806250854276017072460559243537260);
//
x = dbldbl_new(2);
a = atan(x);
assert_checkequal(a.dd(1),1.1071487177940905030170654601785370400700476454014326466765);
//
// Abs
//
a = dbldbl_new ( 2.3 );
b = abs(a);
assert_checkequal(b.dd(1),2.3);
assert_checkequal(b.dd(2),0);
//
a = dbldbl_new ( -2.3 );
b = abs(a);
assert_checkequal(b.dd(1),2.3);
assert_checkequal(b.dd(2),0);
//
// Formatting
//
e = [
  "Double-double:";
  "==============";
  "2.000000000000000000010000000000E0"
  ];
a = dbldbl_new ( 2 , 1.d-20 );
s = string(a);
assert_checkequal(s,e);



